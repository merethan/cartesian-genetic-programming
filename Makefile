# Toolchain
CC      := gcc
CCFLAGS := -Wall -O3 -lm -fopenmp
LDFLAGS :=

# Makes
vfdexperiment: vfdexperiment.c cgp.c cgp.h
	@$(CC) -o vfdexperiment vfdexperiment.c cgp.c $(CCFLAGS)

clean:
	@rm -f cgp.o vfdexperiment *.out

