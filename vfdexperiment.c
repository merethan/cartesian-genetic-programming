/*
CGP experiment by Maarten van Eeuwijk, in an attempt to have
motor controller logic for three-phase AC evolve.
Tnx to Andrew James Turner, Julian F. Miller, Wes Faler
*/

#include <stdio.h>
#include "cgp.h"

int main(void) {

    struct parameters *params = NULL;
    struct dataSet *trainingData = NULL;
    struct chromosome *chromo = NULL;

    int numInputs = 2;
    int numNodes = 125;
    int numOutputs = 3;
    int nodeArity = 2;

    int numGens = 2500000;
    double targetFitness = 0.4;
    int updateFrequency = 10000;

    //double recurrentConnectionProbability = 0.10; # Will be needed once a frequency control mode gets introduced

    params = initialiseParameters(numInputs, numNodes, numOutputs, nodeArity);

    addNodeFunction(params, "add,sub,mul,div,abs,sin,cos,tan,wire");

    setTargetFitness(params, targetFitness);

    setUpdateFrequency(params, updateFrequency);

    //setRecurrentConnectionProbability(params, recurrentConnectionProbability); # Will be needed once a frequency control mode gets introduced

    setMutationRate(params, 0.15); // Default is 0.05

    setNumThreads(params, 4);

    printParameters(params);

    trainingData = initialiseDataSetFromFile("threePhaseMotorController.data");

    chromo = runCGP(params, trainingData, numGens);

    printChromosome(chromo, 0);
    saveChromosomeDot(chromo, 0, "threePhaseMotorControllerChromosome.dot");
    saveChromosomeLatex(chromo, 0, "threePhaseMotorControllerChromosome.tex");

    freeParameters(params);
    freeDataSet(trainingData);
    freeChromosome(chromo);

    return 0;
}
