#! octave -qf
# I'm doing an experiment with Cartesian Genetic Programming, or CGP.
# In this experiment I will attempt to have logic for motor control
# being evolved, to drive a three-phase alternating current electric motor.
#
# The CGP part of the experiment needs input. First step therefore is to
# generate data for it to test the generated code to, in order to evolve using
# selection and mutation. This is not unlike how neural networks "learn".
# Using GNU Octave, a matrix is generated, which will be exported
# to .csv, which will be loaded by the CGP-Library to do its magic on.
#
# This generated data very much functions like a numeric truth-table,
# where desired input and output values are described, which CGP can then go
# figure out the logic for.
#
# Be careful, GNU Octave is indexed at 1! Very much unlike the C-code
# this data will be fed into, where indexes will start at 0.
#
# Size of our (soon to be multi-dimensional) numeric truth-table
# is determined by the inputs. The most important one is rotor position.
# What position the permanent magnet currently is in determines what
# electro-magnet needs to be activated and at what power, to make the motor spin.
# For this purpose, create an array spanning the number of positions the
# sensor mechanism outputs in size, starting at 0 and incrementing by 1 at the time.

rotorres = 24; # Count, most likely a multiplum of 3
rotorpos = 0 : 1 : rotorres - 1; # Starts at 0

# To add another dimension, user input is added: Power output must be
# proportional to power requested from the motor by the user. Not only
# proportional, direction must also match. For this purpose,
# create an array spanning the number of positions the user control mechanism
# outputs in size, starting at 0 and incrementing by 1 at the time.

userres = 255; # Should be an uneven number, as we want to have two even legs from zero
userrespart = (userres -1)/2; # Length of a leg from zero
userpos = (userrespart *-1) : 1 : userrespart;

# As output, we need sine-waves, because three-phase motors like sine-waves,
# hence sine-waves is what a good motor controller must supply.
# To calculate those for our numeric truth-table, Octave expects radials,
# so one full cycle lasts 2pi. Here we divide 2pi into the same number
# of pieces our rotor position sensor mechanism can distinguish,
# matching its resolution.

rotorradials = 0: pi/(rotorres/2) : 2*pi;

# Now this function gets you an array representing a full cicle, meaning
# the first and last element represent the same position on the unity circle.
# Because of this, it yields you rotorres+1 elements; drop the duplicate.

rotorradials = resize(rotorradials, 1, rotorres);

# Now you have rotorres number of elements representing a full sinus cycle,
# each of which can be calculated into a sinus value.
# A three-phase motor controller must output 3 sinuoïds, each separated
# by 120 degrees. Here we calculate a normal sinus, then two offset ones.

outputpwm1 = sin(rotorradials); # Sinus for all previously calculated radial values
outputpwm2 = sin(rotorradials + deg2rad(120)); # A sinus offset by 1/3rd turn of the motor rotor
outputpwm3 = sin(rotorradials - deg2rad(120)); # A sinus offset by 1/3rd turn of the motor rotor

# The produced sinuoïds now have their amplitude in the range of -1 to 1.
# The motor controller hardware however takes a signed integer from -127 to 127
# (no -128 or 0xFF as it may be a trap value), representing a PWM-value
# in either positive or negative direction. Being 255 possible positions total.

pwmres = 255; # Should be an uneven number, as we want to have two even legs from zero
outputpwm1 *= (pwmres -1) / 2;
outputpwm2 *= (pwmres -1) / 2;
outputpwm3 *= (pwmres -1) / 2;

# The numbers representing PWM-value and rotor position are now complete,
# meaning they can now be stacked into a matrix to create a lookup table.
# This table will then be used to test our genetic programming to, after which
# we can start scoring and evolving produced solutions.

simplelut = [rotorpos; outputpwm1; outputpwm2; outputpwm3]; # All i/o rows stacked
simplelut = rot90(simplelut); # Each row is now a truth-table record
simplelut = flipud(simplelut); # First values in arrays are now on top again

# Now the rotor position has been combined with PWM-output to drive the motor,
# essentially we created a lookup table for a program which spins the motor
# faster and faster at maximum power, until the electronics can no longer
# keep up or the motor explodes. Not only that, there's no power control
# input for the user which makes a motor a whole lot less useful.
#
# To include user input into motor output, a lookup table must be made that is
# essentially the simple lookup table in length, multiplied by the number of
# possible user input values, having the motor output scaled
# from zero to full power accordingly.
#
# But, there's a catch: User input can also be a negative value, to spin
# the motor backwards. Inverting all three phases however would crook the
# lookup table by 180 degrees, yet the rotor can only be crooked a multiplum
# of 120 degrees, as there's 3 coils in a circle: To reverse, we
# can't just invert all output, we have to swap two phases.

extendedlut = 0;

for i = userpos # For each possible user position,
  appendtolut = 0; # append a version of the simple lookup table, prefixed with current user input.
  uservalue = i*ones(1, rotorres); # Fill an array with rotorres number of elements containing current user input value
  
  if(i < 0) # User input value is negative number
    appendtolut = [uservalue; rotorpos;
    outputpwm1 * (abs(i) / userrespart); # Make i positive number / resolution as two equal legs from zero
    outputpwm3 * (abs(i) / userrespart); # Phases reversed!
    outputpwm2 * (abs(i) / userrespart)]; # All i/o rows stacked, pwm proportional, phase 2 & 3 reversed
  else # User input value in positive range
    appendtolut = [uservalue; rotorpos;
    outputpwm1 * (i / userrespart); # Resolution as two equal legs from zero
    outputpwm2 * (i / userrespart);
    outputpwm3 * (i / userrespart)]; # All i/o rows stacked, pwm proportional to current userpos (userinput)
  endif

  appendtolut = rot90(appendtolut); # Each row is now a truth-table record
  appendtolut = flipud(appendtolut); # First values in arrays are now on top again
  try
    extendedlut = [extendedlut; appendtolut]; # Append lookup table with PWM values proportional to current user input value i
  catch
    extendedlut = appendtolut; # If append failed (first round matrices don't match in size), overwrite extendedlut
  end_try_catch
endfor

# The truth-tables are now ready for use, except for not having a header yet.
# The header created here is specific to initialiseDataSetFromFile() from CGP-Library.
# Format is rather basic: Number of inputs, number of outputs, total number of records.
simpleheader = [1, 3, rows(simplelut), 0]; # Last cell is padding
simplelut = [simpleheader; simplelut];

extendedheader = [2, 3, rows(extendedlut), 0, 0]; # Last two cells are padding
extendedlut = [extendedheader; extendedlut];

# Output the entire matrices to .csv (or .data if you want to look fancy).
csvwrite("threePhaseMotorControllerSimple.data", simplelut)
csvwrite("threePhaseMotorController.data", extendedlut)

# The generated truth-tables should now satisfy all your machine learning needs!